CREATE DATABASE login;
\c login;
CREATE TABLE users (
    id serial PRIMARY KEY,
    nombre text NOT NULL,
    apellido text NOT NULL,
    nombre_usuario text NOT NULL UNIQUE,
    password text NOT NULL
);

-- password: hola1234 para el usuario amendoza98
INSERT INTO users (nombre, apellido, nombre_usuario, password) 
VALUES ('Alejandro', 'Mendoza', 'amendoza98', '$2y$10$CsVh8mXfdV0Zj7sdwWzEeeurFPXAaTZcVHnDet5nbanYMyxO9GV/e');