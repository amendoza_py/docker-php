<?php
session_start();

if (!isset($_SESSION['nombre'])) {
    header("Location: login.html");
    exit();
}

$nombre = $_SESSION['nombre'];
$apellido = $_SESSION['apellido'];
?>

<!DOCTYPE html>
<html>
<head>
    <title>Bienvenido</title>
    <link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body>
    <div class="welcome-container">
        <h2>Bienvenido <?php echo $nombre; ?> <?php echo $apellido; ?></h2>
        <a href="logout.php">Cerrar Sesión</a>
    </div>
</body>
</html>
