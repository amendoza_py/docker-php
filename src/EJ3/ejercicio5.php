<?php
// Ejercicio 5
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $nombre = $_POST["nombre"];
    $apellido = $_POST["apellido"];
    $edad = $_POST["edad"];
    echo "Nombre: $nombre<br>";
    echo "Apellido: $apellido<br>";
    echo "Edad: $edad<br>";
} else {
    echo <<<HTML
    <form method="POST">
        <label for="nombre">Nombre:</label>
        <input type="text" name="nombre" id="nombre"><br>

        <label for="apellido">Apellido:</label>
        <input type="text" name="apellido" id="apellido"><br>

        <label for="edad">Edad:</label>
        <input type="text" name="edad" id="edad"><br>

        <input type="submit" value="Enviar">
    </form>
HTML;
}
