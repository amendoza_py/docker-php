<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $texto = $_POST["texto"];
    $contrasena = $_POST["contrasena"];
    $selectSimple = $_POST["select_simple"];
    $selectMultiple = $_POST["select_multiple"];
    $checkbox = isset($_POST["checkbox"]) ? "Seleccionado" : "No seleccionado";
    $radio = $_POST["radio"];
    $textarea = $_POST["textarea"];

    echo "Texto: $texto<br>";
    echo "Contraseña: $contrasena<br>";
    echo "Select Simple: $selectSimple<br>";
    echo "Select Múltiple: " . implode(", ", $selectMultiple) . "<br>";
    echo "Checkbox: $checkbox<br>";
    echo "Radio Button: $radio<br>";
    echo "Text Area: $textarea<br>";
}
?>
