<?php
function generarArray() {
    $array = array();
    $letras = 'abcdefghijklmnopqrstuvwxyz';

    for ($i = 0; $i < 10; $i++) {
        $indice = '';
        $longitud = rand(5, 10);

        for ($j = 0; $j < $longitud; $j++) {
            $indice .= $letras[rand(0, strlen($letras) - 1)];
        }

        $valor = rand(1, 1000);
        $array[$indice] = $valor;
    }

    return $array;
}
?>
