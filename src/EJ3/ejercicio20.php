<?php
function buscarEnArchivo($nombre, $apellido, $archivo) {
    $contenido = file_get_contents($archivo);
    $busqueda = $nombre . ' ' . $apellido;

    if (strpos($contenido, $busqueda) !== false) {
        echo "Se encontró $nombre $apellido en el archivo.";
    } else {
        echo "$nombre $apellido no se encontró en el archivo.";
    }
}

if (isset($_POST['nombre']) && isset($_POST['apellido'])) {
    $nombre = $_POST['nombre'];
    $apellido = $_POST['apellido'];
    $archivo = 'agenda.txt';
    buscarEnArchivo($nombre, $apellido, $archivo);
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Búsqueda en Archivo</title>
</head>
<body>
    <form method="post">
        Nombre: <input type="text" name="nombre"><br>
        Apellido: <input type="text" name="apellido"><br>
        <input type="submit" value="Buscar">
    </form>
</body>
</html>
