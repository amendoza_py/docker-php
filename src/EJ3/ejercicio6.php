<!DOCTYPE html>
<html>
<head>
    <title>Calculadora PHP</title>
</head>
<body>
<h1>Calculadora PHP</h1>
<form method="post" action="">
    <label for="operando1">Operando 1:</label>
    <input type="number" name="operando1" required><br>

    <label for="operando2">Operando 2:</label>
    <input type="number" name="operando2" required><br>

    <label for="operacion">Operación:</label>
    <select name="operacion" required>
        <option value="suma">Suma</option>
        <option value="resta">Resta</option>
        <option value="multiplicacion">Multiplicación</option>
        <option value="division">División</option>
    </select><br>

    <input type="submit" name="calcular" value="Calcular">
</form>

<?php
if (isset($_POST['calcular'])) {
    $operando1 = $_POST['operando1'];
    $operando2 = $_POST['operando2'];
    $operacion = $_POST['operacion'];

    $resultado = 0;

    switch ($operacion) {
        case 'suma':
            $resultado = $operando1 + $operando2;
            break;
        case 'resta':
            $resultado = $operando1 - $operando2;
            break;
        case 'multiplicacion':
            $resultado = $operando1 * $operando2;
            break;
        case 'division':
            if ($operando2 != 0) {
                $resultado = $operando1 / $operando2;
            } else {
                echo "Error: No se puede dividir por cero.";
            }
            break;
        default:
            echo "Operación no válida.";
            break;
    }

    echo "Resultado: $resultado";
}
?>
</body>
</html>
