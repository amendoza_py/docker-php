<?php
$archivoContador = 'contador.txt';

function incrementarVisitas($archivo) {
    if (file_exists($archivo)) {
        $contador = (int)file_get_contents($archivo);
        $contador++;
        file_put_contents($archivo, $contador);
    } else {
        file_put_contents($archivo, 1);
    }
}

incrementarVisitas($archivoContador);
$visitas = (int)file_get_contents($archivoContador);

echo "Esta página ha sido visitada $visitas veces.";
?>
