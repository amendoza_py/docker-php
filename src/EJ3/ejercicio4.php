<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1>Ejercicio 4: Ordenar números aleatorios</h1>
    <?php
    // Ejercicio 4
    mt_srand();
    $valor1 = mt_rand(50, 900);
    $valor2 = mt_rand(50, 900);
    $valor3 = mt_rand(50, 900);
    $valores = [$valor1, $valor2, $valor3];
    rsort($valores);

    foreach ($valores as $valor) {
        if ($valor === max($valores)) {
            echo "<span style='color:green'>$valor</span> ";
        } elseif ($valor === min($valores)) {
            echo "<span style='color:red'>$valor</span> ";
        } else {
            echo "$valor ";
        }
    }
    ?>
</body>
</html>
