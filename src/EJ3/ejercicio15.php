<?php

// Definir una constante para el tamaño de la matriz
define('TAMANO_MATRIZ', 3); // Cambia este valor según tus necesidades

function generarMatrizCuadrada($tamano) {
    $matriz = array();
    for ($i = 0; $i < $tamano; $i++) {
        $fila = array();
        for ($j = 0; $j < $tamano; $j++) {
            $fila[] = rand(0, 9);
        }
        $matriz[] = $fila;
    }
    return $matriz;
}

function imprimirMatriz($matriz) {
    foreach ($matriz as $fila) {
        echo implode("\t", $fila) . "\n";
    }
}

function sumaDiagonalPrincipal($matriz) {
    $suma = 0;
    for ($i = 0; $i < count($matriz); $i++) {
        $suma += $matriz[$i][$i];
    }
    return $suma;
}

while (true) {
    $matriz = generarMatrizCuadrada(TAMANO_MATRIZ);
    echo "Matriz generada:\n";
    imprimirMatriz($matriz);
    
    $sumaDiagonal = sumaDiagonalPrincipal($matriz);
    echo "Suma de la diagonal principal: $sumaDiagonal\n";
    
    if ($sumaDiagonal >= 10 && $sumaDiagonal <= 15) {
        echo "La suma de la diagonal principal ($sumaDiagonal) está entre 10 y 15. Finalizando el script.\n";
        break;
    } else {
        echo "La suma de la diagonal principal no está en el rango deseado. Generando otra matriz...\n";
    }
}
?>
