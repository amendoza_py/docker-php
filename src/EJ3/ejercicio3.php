<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1>Ejercicio 3: Tabla de números pares</h1>
    <?php
    // Ejercicio 3
    define('N', 10); // Cambia 10 al valor que desees
    echo "<table border='1'>";
    for ($i = 2; $i <= N; $i += 2) {
        echo "<tr><td>$i</td></tr>";
    }
    echo "</table>";
    ?>
</body>
</html>

