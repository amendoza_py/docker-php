<?php
$arbol = array(
    "valor" => "R",
    "hijos" => array(
        array(
            "valor" => "S",
            "hijos" => array(
                array("valor" => "U", "hijos" => array(
                    array("valor" => "L", "hijos" => array()),
                    array("valor" => "Q", "hijos" => array()),
                    array("valor" => "W", "hijos" => array())
                )),
                array("valor" => "V", "hijos" => array())
            )
        ),
        array(
            "valor" => "T",
            "hijos" => array(
                array("valor" => "G", "hijos" => array(
                    array("valor" => "C", "hijos" => array()),
                    array("valor" => "K", "hijos" => array())
                )),
                array("valor" => "F", "hijos" => array())
            )
        ),
        array(
            "valor" => "D",
            "hijos" => array(
                array("valor" => "H", "hijos" => array(
                    array("valor" => "A", "hijos" => array()),
                    array("valor" => "X", "hijos" => array())
                )),
                array("valor" => "J", "hijos" => array(
                    array("valor" => "Z", "hijos" => array()),
                    array("valor" => "I", "hijos" => array())
                ))
            )
        )
    )
);

function imprimirArbolConSaltosDeLineaHTML($arbol, $nivel = 0) {
    echo str_repeat("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", $nivel) . $arbol["valor"] . "<br>";

    foreach ($arbol["hijos"] as $hijo) {
        imprimirArbolConSaltosDeLineaHTML($hijo, $nivel + 1);
    }
}

imprimirArbolConSaltosDeLineaHTML($arbol);


?>