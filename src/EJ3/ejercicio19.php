<?php
$archivoEntrada = 'datos_alumnos.txt';
$archivoSalida = 'sumatoria_notas.txt';

function calcularSumatoriaNotas($archivoEntrada, $archivoSalida) {
    $lineas = file($archivoEntrada, FILE_IGNORE_NEW_LINES);

    foreach ($lineas as $linea) {
        $datos = explode(',', $linea);
        $matricula = $datos[0];
        $notas = array_slice($datos, 3);
        $sumatoria = array_sum($notas);

        $resultado = "$matricula,$sumatoria";
        file_put_contents($archivoSalida, $resultado . PHP_EOL, FILE_APPEND);
    }
}

calcularSumatoriaNotas($archivoEntrada, $archivoSalida);
echo "Se ha creado el archivo $archivoSalida con la sumatoria de notas.";
?>
