<?php
class Nodo {
    public $valor;
    public $izquierda;
    public $derecha;

    public function __construct($valor) {
        $this->valor = $valor;
        $this->izquierda = null;
        $this->derecha = null;
    }
}

function recorridoPreorden($nodo) {
    if ($nodo === null) {
        return;
    }

    echo $nodo->valor . " ";

    recorridoPreorden($nodo->izquierda);
    recorridoPreorden($nodo->derecha);
}

$raiz = new Nodo(3);
$raiz->izquierda = new Nodo(6);
$raiz->derecha = new Nodo(4);

$raiz->izquierda->izquierda = new Nodo(14);
$raiz->izquierda->derecha = new Nodo(9);

$raiz->derecha->izquierda = new Nodo(900);
$raiz->derecha->derecha = new Nodo(45);

$raiz->izquierda->izquierda->izquierda = new Nodo(100);
$raiz->izquierda->izquierda->derecha = new Nodo(30);

$raiz->izquierda->derecha->izquierda = new Nodo(40);
$raiz->izquierda->derecha->derecha = new Nodo(15);

echo "Recorrido Preorden: ";
recorridoPreorden($raiz);
?>