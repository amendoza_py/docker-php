<!DOCTYPE html>
<html>
<head>
    <title>Contador de Clics en URLs</title>
</head>
<body>
<h1>Contador de Clics en URLs</h1>

<?php
$urls = array(
    'https://www.youtube.com/',
    'https://www.xda-developers.com/',
    'https://www.theverge.com/',
    'https://www.abc.com.py/',
    'https://www.ultimahora.com/',
    'https://www.dw.com/es/',
    'https://www.forbes.com/'
);
?>

<script>
    var contador = {};

    function contarClic(url) {
        if (!contador[url]) {
            contador[url] = 1;
        } else {
            contador[url]++;
        }

        var conteoElement = document.getElementById('conteo_' + url);
        conteoElement.innerHTML = "Clics: " + contador[url];
    }
</script>

<h2>Conteo de Clics:</h2>
<?php
foreach ($urls as $url) {
    echo "<a href='javascript:void(0);' onclick='contarClic(\"$url\");'>$url</a> - <span id='conteo_$url'>Clics: 0</span><br>";
}
?>
</body>
</html>
