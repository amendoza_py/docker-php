<?php
function imprimirClaves($array) {
    $letrasBuscadas = array('a', 'd', 'm', 'z');
    $clavesEncontradas = array();

    foreach ($array as $indice => $valor) {
        $primerCaracter = substr($indice, 0, 1);
        if (in_array($primerCaracter, $letrasBuscadas)) {
            $clavesEncontradas[] = $indice;
        }
    }

    if (empty($clavesEncontradas)) {
        echo "No se encontraron claves que empiecen con 'a', 'd', 'm' o 'z'.";
    } else {
        echo "Claves encontradas: " . implode(', ', $clavesEncontradas);
    }
}
?>
