<!DOCTYPE html>
<html>
<head>
    <title>Verificador de Palíndromos</title>
</head>
<body>
<h1>Verificador de Palíndromos</h1>
<form method="post" action="">
    <label for="cadena">Ingrese una cadena:</label>
    <input type="text" name="cadena" required>
    <input type="submit" name="verificar" value="Verificar">
</form>

<?php
function esPalindromo($cadena) {
    $cadena = strtolower(str_replace(' ', '', $cadena));

    $longitud = strlen($cadena);

    for ($i = 0; $i < $longitud / 2; $i++) {
        if ($cadena[$i] !== $cadena[$longitud - $i - 1]) {
            return false;
        }
    }

    return true;
}

if (isset($_POST['verificar'])) {
    $cadena = $_POST['cadena'];

    if (esPalindromo($cadena)) {
        echo "<p>'$cadena' es un palíndromo.</p>";
    } else {
        echo "<p>'$cadena' no es un palíndromo.</p>";
    }
}
?>
</body>
</html>
