<!DOCTYPE html>
<html>
<head>
    <title>Suma de Elementos en un Vector</title>
</head>
<body>
<h1>Suma de Elementos en un Vector</h1>

<?php
function generarVector($n) {
    $vector = array();

    for ($i = 0; $i < $n; $i++) {
        $vector[] = rand(1, 100);
    }

    return $vector;
}

function sumaVector($vector) {
    return array_sum($vector);
}

$n = 100;
$vector = generarVector($n);
$suma = sumaVector($vector);

echo "<p>Vector generado:</p>";
echo "<pre>" . implode(", ", $vector) . "</pre>";
echo "<p>La suma de los elementos en el vector es: $suma</p>";
?>
</body>
</html>
