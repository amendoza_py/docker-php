<!DOCTYPE html>
<html>
<head>
    <title>Generador de Matriz</title>
</head>
<body>
<h1>Generador de Matriz</h1>
<form method="post" action="">
    <label for="filas">Número de filas (n):</label>
    <input type="number" name="filas" required><br>

    <label for="columnas">Número de columnas (m):</label>
    <input type="number" name="columnas" required><br>

    <input type="submit" name="generar" value="Generar Matriz">
</form>

<?php
function generarMatriz($filas, $columnas) {
    $matriz = array();

    for ($i = 0; $i < $filas; $i++) {
        $fila = array();
        for ($j = 0; $j < $columnas; $j++) {
            $fila[] = rand(1, 100);
        }
        $matriz[] = $fila;
    }

    return $matriz;
}

if (isset($_POST['generar'])) {
    $filas = (int)$_POST['filas'];
    $columnas = (int)$_POST['columnas'];

    $matriz = generarMatriz($filas, $columnas);

    echo "<h2>Matriz Generada:</h2>";
    echo "<table border='1'>";
    foreach ($matriz as $fila) {
        echo "<tr>";
        foreach ($fila as $valor) {
            echo "<td>$valor</td>";
        }
        echo "</tr>";
    }
    echo "</table>";
}
?>
</body>
</html>
