<?php

function buscarCadenaEnArray($cadena, $array) {
    foreach ($array as $valor) {
        if ($valor === $cadena) {
            return true;
        }
    }
    return false;
}

$cadenas = array("cadena1", "cadena2", "cadena3", "cadena4", "cadena5", "cadena6", "cadena7", "cadena8", "cadena9", "cadena10");

$nuevaCadena = "cadena11";

if (buscarCadenaEnArray($nuevaCadena, $cadenas)) {
    echo "Ya existe la cadena \"$nuevaCadena\" en el array.";
} else {
    echo "Es Nuevo. La cadena \"$nuevaCadena\" se ha agregado al final del array.";
    $cadenas[] = $nuevaCadena;
}

echo "<br><pre>";
print_r($cadenas);
echo "</pre>";
?>
