<?php
$elementos10 = array(
    "elemento1" => "valor1",
    "elemento2" => "valor2",
    "elemento3" => "valor3",
    "elemento4" => "valor4",
    "elemento5" => "valor5",
    "elemento6" => "valor6",
    "elemento7" => "valor7",
    "elemento8" => "valor8",
    "elemento9" => "valor9",
    "elemento10" => "valor10"
);

$elemento = array("clave" => "valor");

if (array_key_exists("clave", $elementos10)) {
    echo "La clave existe en el array de 10 elementos.<br>";
} else {
    echo "La clave no existe en el array de 10 elementos.<br>";
}

if (in_array("valor", $elementos10)) {
    echo "El valor existe en el array de 10 elementos.<br>";
} else {
    echo "El valor no existe en el array de 10 elementos.<br>";
}

if (!array_key_exists("clave", $elementos10) && !in_array("valor", $elementos10)) {
    $elementos10["clave"] = "valor";
}

echo "<pre>";
print_r($elementos10);
echo "</pre>";
?>
