<!DOCTYPE html>
<html>
<head>
    <title>Elemento con Mayor Valor en un Vector</title>
</head>
<body>
<h1>Elemento con Mayor Valor en un Vector</h1>

<?php
function generarVector($n) {
    $vector = array();

    for ($i = 0; $i < $n; $i++) {
        $vector[] = rand(1, 1000);
    }

    return $vector;
}

function encontrarMayor($vector) {
    $mayorValor = max($vector);
    $indice = array_search($mayorValor, $vector);
    return array('valor' => $mayorValor, 'indice' => $indice);
}

$n = 50;
$vector = generarVector($n);
$resultado = encontrarMayor($vector);

echo "<p>Vector generado:</p>";
echo "<pre>" . implode(", ", $vector) . "</pre>";
echo "El elemento con índice " . $resultado['indice'] . " posee el mayor valor que " . $resultado['valor'] . ".";
?>
</body>
</html>

