<!DOCTYPE html>
<html>
<head>
    <title>Formulario PHP</title>
</head>
<body>
    <form method="post" action="procesar_formulario.php">
        Texto: <input type="text" name="texto"><br>
        Contraseña: <input type="password" name="contrasena"><br>
        Select Simple:
        <select name="select_simple">
            <option value="opcion1">Opción 1</option>
            <option value="opcion2">Opción 2</option>
            <option value="opcion3">Opción 3</option>
        </select><br>
        Select Múltiple:
        <select name="select_multiple[]" multiple>
            <option value="opcionA">Opción A</option>
            <option value="opcionB">Opción B</option>
            <option value="opcionC">Opción C</option>
        </select><br>
        Checkbox: <input type="checkbox" name="checkbox" value="Si"><br>
        Radio Button 1: <input type="radio" name="radio" value="Opción 1"><br>
        Radio Button 2: <input type="radio" name="radio" value="Opción 2"><br>
        Text Area:<br>
        <textarea name="textarea" rows="4" cols="50"></textarea><br>
        <input type="submit" value="Enviar">
    </form>
</body>
</html>
