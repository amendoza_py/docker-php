<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1>Ejercicio 2: Información de PHP</h1>
    <?php
    // Ejercicio 2
    $phpVersion = phpversion();
    $phpId = php_uname('v');
    $maxInt = PHP_INT_MAX;
    $maxFileNameLength = PHP_MAXPATHLEN;
    $osVersion = php_uname('s');
    $eolSymbol = PHP_EOL;
    $includePath = get_include_path();

    echo "Versión de PHP utilizada: $phpVersion<br>";
    echo "ID de la versión de PHP: $phpId<br>";
    echo "Valor máximo soportado para enteros: $maxInt<br>";
    echo "Tamaño máximo del nombre de un archivo: $maxFileNameLength<br>";
    echo "Versión del Sistema Operativo: $osVersion<br>";
    echo "Símbolo correcto de 'Fin De Línea': $eolSymbol<br>";
    echo "Include path por defecto: $includePath<br>";
    ?>
</body>
</html>
