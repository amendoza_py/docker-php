<!-- ejercicio 4 -->
<!DOCTYPE html>
<html>
<head>
<style>
  table {
    border-collapse: collapse;
    width: 50%;
    margin: 0 auto;
    border: 2px solid black;
  }

  th, td {
    padding: 8px;
    text-align: left;
    border: 1px solid black;
    
  }

  .th2 {
    background-color: yellow;
    text-align: center;
  }

  .productos {
    background-color: #ccc;
  }

  .subcolumn {
    background-color: #808080;
    text-align: center;
  }

  .odd-row {
    background-color: #e8ecdc;
  }

  .even-row {
    background-color: white;
  }
</style>
</head>
<body>

<?php
$datos = array(
  array("Cocacola", 100, 4500),
  array("Pepsi", 30, 4800),
  array("Sprite", 20, 4500),
  array("Guarana", 200, 4500),
  array("SevenUp", 24, 4800),
  array("Mirinda Naranja", 56, 4800),
  array("Mirinda Guarana", 89, 4800),
  array("Fanta Naranja", 10, 4500),
  array("Fanta Piña", 2, 4500),

  
);

echo '<table>';
echo '<tr><th class="th2" colspan="3">PRODUCTOS</th></tr>';
echo '<tr class="subcolumn"><th>Nombre</th><th>Cantidad</th><th>Precio</th></tr>';

foreach ($datos as $indice => $fila) {
  $clase_fila = ($indice % 2 == 0) ? 'even-row' : 'odd-row';
  echo '<tr class="' . $clase_fila . '">';
  foreach ($fila as $valor) {
    echo '<td>' . $valor . '</td>';
  }
  echo '</tr>';
}

echo '</table>';
?>

</body>
</html>
