<!DOCTYPE html>
<html>
<head>
    <title>Información Personal</title>
</head>
<body>
    <h1>Ingrese su información</h1>
    <form method="post" action="">
        <label for="nombre">Nombre y Apellido:</label>
        <input type="text" name="nombre" id="nombre" required><br><br>

        <label for="nacionalidad">Nacionalidad:</label>
        <input type="text" name="nacionalidad" id="nacionalidad" required><br><br>

        <input type="submit" name="imprimir" value="Imprimir">
    </form>
    
    <?php
    if (isset($_POST["imprimir"])) {
        $nombre = $_POST["nombre"];
        $nacionalidad = $_POST["nacionalidad"];
        echo "<p><strong><span style='color: red;'>$nombre</span></strong></p>";
        echo "<p><u>$nacionalidad</u></p>";
    }
    ?>
</body>
</html>
