<?php
// Generar valores aleatorios para las notas de los parciales y el final
$parcial1 = rand(0, 30);
$parcial2 = rand(0, 20);
$final1 = rand(0, 50);

// Calcular la suma de los acumulados
$acumulado = $parcial1 + $parcial2 + $final1;

// Calcular la nota en base al acumulado usando la estructura switch
$nota = '';
switch ($acumulado) {
    case ($acumulado >= 0 && $acumulado <= 59):
        $nota = 'Nota 1';
        break;
    case ($acumulado >= 60 && $acumulado <= 69):
        $nota = 'Nota 2';
        break;
    case ($acumulado >= 70 && $acumulado <= 79):
        $nota = 'Nota 3';
        break;
    case ($acumulado >= 80 && $acumulado <= 89):
        $nota = 'Nota 4';
        break;
    case ($acumulado >= 90 && $acumulado <= 100):
        $nota = 'Nota 5';
        break;
    default:
        $nota = 'Fuera de rango';
}

// Imprimir la nota final
echo "El acumulado es: $acumulado<br>";
echo "La nota del alumno es: $nota";
?>
