<!DOCTYPE html>
<html>
<head>
    <title>Tabla de Multiplicar del 9</title>
    <style>
        .fila-par {
            background-color: #f2f2f2; 
        }

        .fila-impar {
            background-color: #ffffff;
        }
    </style>
</head>
<body>
    <h1>Tabla de Multiplicar del 9</h1>
    <table>
        <thead>
            <tr>
                <th>Numero</th>
                <th>Resultado</th>
            </tr>
        </thead>
        <tbody>
            <?php
            for ($i = 1; $i <= 10; $i++) {
                $clase_fila = ($i % 2 == 0) ? "fila-par" : "fila-impar";
                $resultado = 9 * $i;
                echo "<tr class='$clase_fila'>";
                echo "<td>$i</td>";
                echo "<td>$resultado</td>";
                echo "</tr>";
            }
            ?>
        </tbody>
    </table>
</body>
</html>
