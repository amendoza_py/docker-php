<!DOCTYPE html>
<html>
<head>
    <title>Concatenación de Cadenas en PHP</title>
</head>
<body>
    <h1>Concatenación de Cadenas en PHP</h1>

    <?php
    $cadena1 = "Hola, ";
    $cadena2 = "Mundo!";
    $resultado = $cadena1 . $cadena2;
    echo "<p>$resultado</p>";
    ?>
</body>
</html>
