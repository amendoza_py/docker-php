<!DOCTYPE html>
<html>
<head>
    <title>Tipos de Datos en PHP</title>
</head>
<body>
    <h1>Tipos de Datos en PHP</h1>

    <?php
    $variable = 24.5;
    echo "El tipo de datos de la variable es: " . gettype($variable) . "<br>";

    $variable = "HOLA";
    echo "El tipo de datos de la variable es: " . gettype($variable) . "<br>";
    settype($variable, "integer");

    echo "El contenido y tipo de la variable después del cambio es: ";
    var_dump($variable);
    ?>
</body>
</html>
