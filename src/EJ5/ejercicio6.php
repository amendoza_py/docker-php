<!DOCTYPE html>
<html>
<head>
    <title>Mapa de Google Maps</title>
    <style>
        #map {
            height: 400px;
            width: 100%;
        }
    </style>
</head>
<body>
<h1>Mapa de Google Maps</h1>

<div id="map"></div>

<script>
    function initMap() {
        var coordenadas = {lat: 37.7749, lng: -122.4194};

        // Crear un objeto de mapa
        var map = new google.maps.Map(document.getElementById('map'), {
            center: coordenadas,
            zoom: 12 // Nivel de zoom
        });

        var marker = new google.maps.Marker({
            position: coordenadas,
            map: map,
            title: 'Ubicación'
        });
    }
</script>

<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDLBlxknbAOuAElMvTKjwe-kluPwjRB7Uo&callback=initMap">
</script>
</body>
</html>
