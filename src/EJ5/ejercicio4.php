<?php
$host = 'postgres';
$port = '5432';
$dbname = 'ejercicio1';
$user = 'postgres';
$password = 'postgres';

try {
    $dsn = "pgsql:host=$host;port=$port;dbname=$dbname";
    $pdo = new PDO($dsn, $user, $password);

    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $query = "SELECT p.nombre AS nombre_producto, p.precio AS precio_producto, m.nombre AS nombre_marca, e.nombre AS nombre_empresa, c.nombre AS nombre_categoria
              FROM \"Producto\" as p
              INNER JOIN \"Marca\" AS m ON p.id_marca = m.id_marca
              INNER JOIN \"Categoria\" AS c ON p.id_categoria = c.id_categoria
              INNER JOIN \"Empresa\" AS e ON m.id_empresa = e.id_empresa";

    $stmt = $pdo->query($query);

    echo "<table border='1'>
            <tr>
                <th>Nombre del Producto</th>
                <th>Precio del Producto</th>
                <th>Nombre de la Marca</th>
                <th>Nombre de la Empresa</th>
                <th>Nombre de la Categoría</th>
            </tr>";

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        echo "<tr>";
        echo "<td>" . $row['nombre_producto'] . "</td>";
        echo "<td>" . $row['precio_producto'] . "</td>";
        echo "<td>" . $row['nombre_marca'] . "</td>";
        echo "<td>" . $row['nombre_empresa'] . "</td>";
        echo "<td>" . $row['nombre_categoria'] . "</td>";
        echo "</tr>";
    }

    echo "</table>";
} catch (PDOException $e) {
    die("Error en la conexión o consulta a la base de datos: " . $e->getMessage());
}
