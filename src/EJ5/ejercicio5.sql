-- public.tabla definition

-- Drop table

-- DROP TABLE tabla;

CREATE TABLE tabla (
                       id serial4 NOT NULL,
                       nombre varchar(40) NULL,
                       descripcion varchar NULL,
                       CONSTRAINT tabla_pk PRIMARY KEY (id)
);