CREATE TABLE "Producto" (
  "id_producto" serial PRIMARY KEY,
  "id_marca" int4,
  "id_categoria" int4,
  "nombre" varchar(100),
  "precio" decimal
);

CREATE TABLE "Categoria" (
  "id_categoria" serial PRIMARY KEY,
  "nombre" varchar(100)
);

CREATE TABLE "Marca" (
  "id_marca" serial PRIMARY KEY,
  "id_empresa" int4,
  "nombre" varchar(100)
);

CREATE TABLE "Empresa" (
  "id_empresa" serial PRIMARY KEY,
  "nombre" varchar(100)
);

ALTER TABLE "Producto" ADD FOREIGN KEY ("id_marca") REFERENCES "Marca" ("id_marca");

ALTER TABLE "Producto" ADD FOREIGN KEY ("id_categoria") REFERENCES "Categoria" ("id_categoria");

ALTER TABLE "Marca" ADD FOREIGN KEY ("id_empresa") REFERENCES "Empresa" ("id_empresa");


INSERT INTO public."Categoria" (id_categoria, nombre) VALUES(1, 'Gaseosa');

INSERT INTO public."Empresa" (id_empresa, nombre) VALUES(1, 'Coca Cola Company');

INSERT INTO public."Marca" (id_marca, id_empresa, nombre) VALUES(1, 1, 'Fanta');
INSERT INTO public."Marca" (id_marca, id_empresa, nombre) VALUES(2, 1, 'Coca Cola');
INSERT INTO public."Marca" (id_marca, id_empresa, nombre) VALUES(3, 1, 'Paso de los Toros');
INSERT INTO public."Marca" (id_marca, id_empresa, nombre) VALUES(4, 1, 'SCHWEPPES ');

INSERT INTO public."Producto" (id_producto, id_marca, id_categoria, nombre, precio) VALUES(2, 4, 1, 'GASEOSA CITRUS SCHWEPPES BOTELLA 2', 12500);
INSERT INTO public."Producto" (id_producto, id_marca, id_categoria, nombre, precio) VALUES(3, 2, 1, 'GASEOSA SIN AZUCARES DESCARTABLE COCA COLA 3LT
', 17000);
INSERT INTO public."Producto" (id_producto, id_marca, id_categoria, nombre, precio) VALUES(4, 1, 1, 'GASEOSA SABOR GUARANA DESCARTABLE FANTA 2LT
', 12500);
INSERT INTO public."Producto" (id_producto, id_marca, id_categoria, nombre, precio) VALUES(5, 3, 1, 'GASEOSA SABOR POMELO PASO DE LOS TOROS 2LT
', 10900);
