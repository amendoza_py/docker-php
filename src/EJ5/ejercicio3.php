<?php
$host = 'postgres';
$port = '5432';
$dbname = 'ejercicio1';
$user = 'postgres';
$password = 'postgres';

$connection = pg_connect("host=$host port=$port dbname=$dbname user=$user password=$password");

if (!$connection) {
    die("Error al conectarse a la base de datos: " . pg_last_error());
}

$query = "SELECT p.nombre AS nombre_producto, p.precio AS precio_producto, m.nombre AS nombre_marca, e.nombre AS nombre_empresa, c.nombre AS nombre_categoria
          FROM \"Producto\" as p
          INNER JOIN \"Marca\" AS m ON p.id_marca = m.id_marca
          INNER JOIN \"Categoria\" AS c ON p.id_categoria = c.id_categoria
          INNER JOIN \"Empresa\" AS e ON m.id_empresa = e.id_empresa";

$result = pg_query($connection, $query);

if (!$result) {
    die("Error en la consulta: " . pg_last_error());
}

echo "<table border='1'>
        <tr>
            <th>Nombre del Producto</th>
            <th>Precio del Producto</th>
            <th>Nombre de la Marca</th>
            <th>Nombre de la Empresa</th>
            <th>Nombre de la Categoría</th>
        </tr>";

while ($row = pg_fetch_assoc($result)) {
    echo "<tr>";
    echo "<td>" . $row['nombre_producto'] . "</td>";
    echo "<td>" . $row['precio_producto'] . "</td>";
    echo "<td>" . $row['nombre_marca'] . "</td>";
    echo "<td>" . $row['nombre_empresa'] . "</td>";
    echo "<td>" . $row['nombre_categoria'] . "</td>";
    echo "</tr>";
}

echo "</table>";

pg_close($connection);
?>
