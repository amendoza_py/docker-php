<?php
$host = 'postgres';
$port = '5432';
$dbname = 'ejercicio4';
$user = 'postgres';
$password = 'postgres';

try {
    $dsn = "pgsql:host=$host;port=$port;dbname=$dbname";
    $pdo = new PDO($dsn, $user, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $insertSQL = "INSERT INTO tabla (nombre, descripcion) VALUES (:nombre, :descripcion)";
    $stmt = $pdo->prepare($insertSQL);
    for ($i = 1; $i <= 1000; $i++) {
        $nombre = 'Registro ' . $i;
        $descripcion = 'Descripción del registro ' . $i;
        $stmt->bindParam(':nombre', $nombre);
        $stmt->bindParam(':descripcion', $descripcion);
        $stmt->execute();
    }

    echo "<table border='1'>
        <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Descripción</th>
        </tr>";

    $selectSQL = "SELECT * FROM tabla";
    $result = $pdo->query($selectSQL);

    foreach ($result as $row) {
        echo "<tr>";
        echo "<td>" . $row['id'] . "</td>";
        echo "<td>" . $row['nombre'] . "</td>";
        echo "<td>" . $row['descripcion'] . "</td>";
        echo "</tr>";
    }

    echo "</table>";
} catch (PDOException $e) {
    die("Error en la conexión o consulta a la base de datos: " . $e->getMessage());
}

