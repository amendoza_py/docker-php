FROM php:8.2-apache

# Instala las dependencias necesarias
RUN apt-get update && apt-get install -y libpq-dev postgresql-client

# Habilita la extensión PDO PostgreSQL
RUN docker-php-ext-configure pdo_pgsql --with-pdo-pgsql=/usr/local/pgsql && docker-php-ext-install pdo pdo_pgsql

# Copia tus archivos de la aplicación
COPY src/ /var/www/html/

# Expone el puerto 80
EXPOSE 80